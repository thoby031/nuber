import logo from "./logo.svg";
import "@aws-amplify/ui-react/styles.css";
import "./App.css"
import driver from "./nuber-drive.png"
import { API, Storage } from 'aws-amplify';


import {
  withAuthenticator,
  Button,
  Heading,
  Image,
  View,
  Card,
} from "@aws-amplify/ui-react";

function App({signOut}) {

  // async function fetchNotes() {
  //   const apiData = await API.graphql({ query: listNotes });
  //   const notesFromAPI = apiData.data.listNotes.items;
  //   await Promise.all(notesFromAPI.map(async note => {
  //     if (note.image) {
  //       const image = await Storage.get(note.image);
  //       note.image = image;
  //     }
  //     return note;
  //   }))
  //   setNotes(apiData.data.listNotes.items);
  // }



  // async function createNote() {
  //   if (!formData.name || !formData.description) return;
  //   await API.graphql({ query: createNoteMutation, variables: { input: formData } });
  //   if (formData.image) {
  //     const image = await Storage.get(formData.image);
  //     formData.image = image;
  //   }
  //   setNotes([ ...notes, formData ]);
  //   setFormData(initialFormState);
  // }



  return (
    <div className='App'>
      <View className="App">
      
    </View>
      <div className='banner'>
        <h1 className='logo'>Nuber</h1>
        <h2 className='type'>Driver</h2>
        <Button onClick={signOut} className='signOut'>Sign Out</Button>
        <Button className='photo-btn'>Upload Photo</Button>
        
      </div>
      <img src={driver} alt="nuber-drive.png" className='nuber-driver'></img>
      <div className='bottom-banner'></div>
      
      
        
  </div>

    
  );
  
}

export default withAuthenticator(App);
